function App() {
  this.elUsername = document.getElementById("username");
  this.elPassword = document.getElementById("password");
  this.elBtnLogin = document.getElementById("btn-login");
}

App.prototype.init = function () {
  //check for token in cookie

  //verify cookie

  //redirect to dashboard

  this.elBtnLogin.addEventListener("click", async (e) => {
    // Prevent the form from Submitting.
    e.preventDefault();

    if (!this.elUsername.value || !this.elPassword.value) {
      return alert("Please enter valid inputs.");
    }

    try {
      const result = await fetch("http://localhost:3000/api/login", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          username: this.elUsername.value.trim(),
          password: this.elPassword.value.trim(),
        }),
      }).then((r) => r.json());

      if (result.status >= 400) {
        alert(result.message);
        return;
      }
      // 1. Store the token in a cookie called token.
      document.cookie = "token=" + result.token;

      // 2. Redirect to Dashboard.
      window.location.href = "/dashboard";
    } catch (e) {
      alert(e.toString());
    }
  });
};

new App().init();
