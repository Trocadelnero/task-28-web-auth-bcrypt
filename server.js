const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const bcrypt = require("bcrypt");
const { PORT = 3000 } = process.env;
const { Sequelize, QueryTypes } = require("sequelize");
require("dotenv").config();

// Initialize the App.
const app = express();

// Setup Middleware applications.
app.use(express.json());
app.use(cookieParser());
// Expose JavaScript, Images and CSS files under the alias public/
app.use("/public", express.static(path.join(__dirname, "www")));

// initialize the database connection
const sequelize = new Sequelize({
  database: process.env.DB_DATABASE,
  username: process.env.DB_USER,
  password: process.env.DB_PASS,
  host: process.env.DB_HOST,
  dialect: process.env.DB_DIALECT,
});

// Check if we can establish a connection to the database.
sequelize
  .authenticate()
  .then((d) => {
    // Successfully connected to the Database.
    app.use(async (req, res, next) => {
      //  NOTE: This is an async function -> You can use await.

      // 1. Allow / to pass through
      // If user tries to load /dashboard it should redirect to / -- See slides.
      const publicPaths = ["/", "/api/login", "/api/register"]; //Public paths // Forgot the '/' before api/register, gave index.html as response for register.post in postman.

      if (publicPaths.includes(req.path)) {
        return next();
      }
      // 2. Check if the req cookie -> token exists.
      if (!req.cookies.token) {
        return res.redirect("/"); //'back'?
      }
      // 3. get the token from the request cookie
      const { token } = req.cookies;

      try {
        // 4. Check the database for the token and if it is active = 1
        const dbToken = await sequelize.query(
          "SELECT * FROM usersession WHERE token = :token AND active = 1",
          {
            type: QueryTypes.SELECT,
            replacements: {
              token: token,
            },
          }
        );

        if (dbToken.length === 0) { // wrote .lenght for some reason + 2 hours troubleshooting. - resulted in token 'undefined'
          return res
            .status(403)
            .send(
              'Missing Authorization to view this page. <br> <a href="/">Back to Login</a>'
            );
        }
        // 5. Check if the user linked to the token exists

        // 6. Return a status 401 If it is expired or the user does not Exists

        // Should be the last line of code here.
      } catch (e) {}

      return next();
    });

    app.get("/", (req, res) => {
      // this is our index page (or login)
      return res
        .status(200)
        .sendFile(path.join(__dirname, "www", "index.html"));
    });

    app.get("/dashboard", (req, res) => {
      // Must be a protected page. Can't access without token cookie.
      return res
        .status(200)
        .sendFile(path.join(__dirname, "www", "dashboard.html"));
    });

    app.post("/api/register", async (req, res) => {
      const username = "Admin"; //I Had an extra space here "Admin " +3 hours of troubleshooting - 
      const password = "sup3rs3cr3t";
      try {
        const hashPassword = await bcrypt.hash(password, 10);
        const result = await sequelize.query(
          "INSERT INTO user (username, password) VALUE (:username, :password)",
          {
            type: QueryTypes.INSERT,
            replacements: {
              username: username,
              password: hashPassword,
            },
          }
        );
      } catch (e) {
        return res.json(e);
      }
      return res.json("Done...");
    });

    app.post("/api/login", async (req, res) => {
      // Authentication Request.

      // Check login
      // 1. Get the username and password from the req.body
      const { username, password } = req.body;

      if (!username || !password) {
        return res.status(400).json({
          message: "Please provide a username & password",
        });
      }

      try {
        // 2. Check if the username exists (Select the user based on the username)
        const dbUser = await sequelize.query(
          "SELECT * FROM user WHERE username = :username",
          {
            type: QueryTypes.SELECT,
            replacements: {
              username: username,
            },
          }
        );

        if (dbUser && dbUser.length == 0) {
          return res.status(401).json({
            message: "Sorry, that user does not exist!",
          });
        }
        // 3. Check if the password in the db matches the password given.
        // NOTE: use bcrypt (Already required at the top) - To hash the password with 10 rounds.
        // For help see: https://www.npmjs.com/package/bcrypt
        // Check the section on Promises:
        // Load hash from your password DB. Sample using Async/Await:
        const validPassword = await bcrypt.compare(
          password,
          dbUser[0].password
        );

        if (!validPassword) {
          return res.status(401).json({
            message: "Invalid credentials!",
          });
        }

        // 4. If true Generate a token using the HASH_SECRET from the .env file (it will be on the process.env)
        const token = await bcrypt.hash(process.env.HASH_SECRET, 10);

        const tokenResult = await sequelize.query(
          "INSERT INTO usersession ( userId, token ) VALUE (:userId, :token)",
          {
            type: QueryTypes.INSERT,
            replacements: {
              userId: dbUser[0].userId,
              token: token,
            },
          }
        );

        // 5. NB - Send the token back to the client.

        return res.status(201).json({
          status: 201,
          token: token,
        });
      } catch (e) {
        return res.status(500).json({
          message: e.toString(),
        });
      }
    });

    app.listen(PORT, () =>
      console.log(`Server has started on port ${PORT}...`)
    );
  })
  .catch((e) => {
    console.error(e);
  });
